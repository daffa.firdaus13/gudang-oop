-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2019 at 05:27 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `IdAkun` int(5) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(30) NOT NULL,
  `Sex` varchar(5) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `flag_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`IdAkun`, `Username`, `Password`, `Sex`, `Phone`, `Email`, `status`, `flag_active`) VALUES
(1, 'admin', 'admin', 'Men', '1111', 'admin@admin.com', 1, 1),
(2, 'Hadits', '1234', 'Men', '081345441', 'hadits@gmail.com', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `noG` int(11) NOT NULL,
  `kodeB` char(4) NOT NULL,
  `namaB` varchar(256) NOT NULL,
  `stock` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`noG`, `kodeB`, `namaB`, `stock`) VALUES
(1, '0001', 'kursi', '10');

-- --------------------------------------------------------

--
-- Table structure for table `log_in`
--

CREATE TABLE `log_in` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `image` varchar(128) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `data_created` datetime NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_in`
--

INSERT INTO `log_in` (`id`, `name`, `email`, `password`, `image`, `role_id`, `is_active`, `data_created`, `code`) VALUES
(7, 'didi', 'lola@gmail.com', '$2y$10$SzKqOGnwwJBC2NK/N04zHOKxGsoVfLu6CXMOnTigvPgVRsb98KYZS', '', 0, 1, '0000-00-00 00:00:00', 'dNCqFEmMP9'),
(18, 'Muhammad Hadits A', 'haditsalkhafidl@gmail.com', '$2y$10$EhJ2b4aQ6HXOfESrPOX8tOLY/DNOiBu.eDLzRBmPMve6aWK5X13KG', '', 1, 1, '2019-07-03 19:51:18', 'halVC2btAu'),
(25, 'Dolpinus ', 'dolpinus_erektus@gmail.com', '$2y$10$UPXAOeMmOF1qdUJcZnPWj.arkdkayHp9k/gLuNCUtcsDJwbswFN46', '', 0, 1, '0000-00-00 00:00:00', 'zje6aCGXrM'),
(26, 'lili', 'lili@gmail.com', '1234', '0', 1, 1, '0000-00-00 00:00:00', ''),
(27, 'lili', 'lili123@gmail.com', '$2y$10$hn.XBBFPG7Xsnkvv4hhFG.soBRYS1fRz1AjQFdAy1RaVrBlh3BwNG', '', 0, 0, '0000-00-00 00:00:00', 'ALK4WvuHge'),
(28, 'lolita', 'lolita12@gmail.com', '$2y$10$JopqS5n10qH/.W84Fjepx.sUvGXAJ0q41RWJg5.FaZx7r7tE3SYBO', '', 0, 0, '0000-00-00 00:00:00', 'Y2ow9hZIlM'),
(29, 'dania', 'audrey.dania23@gmail.com', '$2y$10$Jv3Lu8MWm0GMm2SwJxGOd.Au3wpAtkCOzdiUXhZfsguYsq8fk5Pbu', '', 1, 1, '2019-11-09 20:57:43', 'siDKFhrUk4'),
(30, 'kontol', 'hadizt339@gmail.com', '$2y$10$eB4sizDTnohlTapFrg24beeVh65HDv774mxFBCi26S6vY5SN4Cnc2', '', 0, 0, '2019-11-09 21:06:41', 'TDfNivadWr');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'administrator'),
(2, 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`IdAkun`,`Username`),
  ADD UNIQUE KEY `Username` (`Username`),
  ADD UNIQUE KEY `Username_2` (`Username`),
  ADD KEY `IdAkun` (`IdAkun`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`noG`);

--
-- Indexes for table `log_in`
--
ALTER TABLE `log_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `IdAkun` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `noG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_in`
--
ALTER TABLE `log_in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
