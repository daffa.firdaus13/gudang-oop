/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Koneksi {
    private static Connection mysqlKoneksi;
        public Koneksi(){
    }
    public static Connection bukaKoneksi() throws SQLException{
        Connection kon = null;
        try{
            Class.forName("org.gjt.mm.mysql.Driver");
            kon = DriverManager.getConnection("jdbc:mysql://localhost/login", "root", "");
            return kon;
        }
        catch(SQLException sqlEx){
            System.out.println("Database tidak dapat dibuka");
            return null;
        }
        catch(Exception ex){
            System.out.println("Anda tidak terhubung ke database");
            return null;
        }
    }
    public static void main(String args[]){
        new Koneksi();
    }
}
